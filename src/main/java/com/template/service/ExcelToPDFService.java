package com.template.service;

import com.spire.xls.Workbook;
import com.spire.xls.Worksheet;

import java.io.IOException;

public class ExcelToPDFService {

    private static final String PDF_PATH = "test2.pdf";

    public void saveExcelToPDF(String excelFilePath) throws IOException {
        Workbook workbook = new Workbook();
        workbook.loadFromFile(excelFilePath);
        Worksheet worksheet = workbook.getWorksheets().get(0);

        worksheet.saveToPdf(PDF_PATH);
    }
}
