package com.template.bootstrap;

import com.template.model.Book;

import java.util.Arrays;
import java.util.List;

public class BookDataLoader {

    public List<Book> getBooks() {
        Book book1 = new Book("Title1", "Author1", 50.5);
        Book book2 = new Book("Title2", "Author2", 60.5);
        Book book3 = new Book("Title3", "Author3", 70.5);
        Book book4 = new Book("Title4", "Author4", 80.5);
        Book book5 = new Book("Title5", "Author5", 90.5);

        return Arrays.asList(book1, book2, book3, book4, book5);
    }
}
