package com.template.service;

import com.template.model.Book;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class ExcelService {

    private static final String COULD_NOT_WRITE_CONTENT_ERROR_MESSAGE = "Couldn't write content to path %s";
    private static final String COULD_NOT_READ_CONTENT_ERROR_MESSAGE = "Couldn't read content from path %s";
    private static final String AUTHOR = "Author";
    private static final String TITLE = "Title";
    private static final String PRICE = "Price";

    public void readExcel(String excelFilePath) {

        try (FileInputStream fis = new FileInputStream(excelFilePath)) {
            HSSFWorkbook workbook = new HSSFWorkbook(fis);

            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    printCellContentByType(cell);
                }
                System.out.println();
            }
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_READ_CONTENT_ERROR_MESSAGE, excelFilePath));
        }
    }

    public void writeExcel(List<Book> books, String excelFilePath) {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        int rowCount = 0;
        createHeaderRow(sheet);

        for (Book book : books) {
            Row row = sheet.createRow(++rowCount);
            writeBook(book, row);
        }

        try (FileOutputStream fos = new FileOutputStream(excelFilePath)) {
            workbook.write(fos);
        } catch (IOException e) {
            System.out.println(String.format(COULD_NOT_WRITE_CONTENT_ERROR_MESSAGE, excelFilePath));
        }
    }

    private void printCellContentByType(Cell cell) {
        switch (cell.getCellType()) {
            case NUMERIC:
                System.out.print(cell.getNumericCellValue() + " ");
                break;
            case STRING:
                System.out.print(cell.getStringCellValue() + " ");
                break;
        }
    }

    private void writeBook(Book book, Row row) {
        Cell cell = row.createCell(1);
        cell.setCellValue(book.getTitle());

        cell = row.createCell(2);
        cell.setCellValue(book.getAuthor());

        cell = row.createCell(3);
        cell.setCellValue(book.getPrice());
    }

    private void createHeaderRow(Sheet sheet) {
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 16);
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);
        Cell cellTitle = row.createCell(1);
        cellTitle.setCellStyle(cellStyle);
        cellTitle.setCellValue(TITLE);

        Cell cellAuthor = row.createCell(2);
        cellAuthor.setCellStyle(cellStyle);
        cellAuthor.setCellValue(AUTHOR);

        Cell cellPrice = row.createCell(3);
        cellPrice.setCellStyle(cellStyle);
        cellPrice.setCellValue(PRICE);
    }
}
