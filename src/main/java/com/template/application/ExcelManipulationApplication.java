package com.template.application;

import com.template.bootstrap.BookDataLoader;
import com.template.service.ExcelService;
import com.template.service.ExcelToPDFService;

import java.io.IOException;

public class ExcelManipulationApplication {

    private static final ExcelToPDFService EXCEL_TO_PDF_SERVICE = new ExcelToPDFService();
    private static final BookDataLoader BOOK_DATA_LOADER = new BookDataLoader();
    private static final ExcelService EXCEL_SERVICE = new ExcelService();
    private static final String EXCEL_WRITE_FILE_PATH = "writeTest.xls";
    private static final String EXCEL_READ_FILE_PATH = "readTest.xls";
    private static final String EXCEL_TO_PDF_FILE_PATH = "test.xlsx";

    public static void main(String[] args) throws IOException {
        EXCEL_SERVICE.writeExcel(BOOK_DATA_LOADER.getBooks(), EXCEL_WRITE_FILE_PATH);
        EXCEL_SERVICE.readExcel(EXCEL_READ_FILE_PATH);
        EXCEL_TO_PDF_SERVICE.saveExcelToPDF(EXCEL_TO_PDF_FILE_PATH);
    }
}
